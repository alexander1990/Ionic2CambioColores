import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Color } from '../../models/colors'
import { Coloram } from '../coloram/coloram';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  color= new Color();

  constructor(public navCtrl: NavController) {

  }

  public cargar(){
  	 this.navCtrl.setRoot(Coloram, {
  	 	color: this.color
  	 });
  }

}
