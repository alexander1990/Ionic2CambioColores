import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Coloram } from './coloram';

@NgModule({
  declarations: [
    Coloram,
  ],
  imports: [
    IonicPageModule.forChild(Coloram),
  ],
})
export class ColoramModule {}
