import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { Color } from '../../models/colors';
import { HomePage } from '../home/home';
@Component({
  selector: 'page-coloram',
  templateUrl: 'coloram.html',
})
export class Coloram {
  code_color: string;
  time:number;
  interval: number = 1;
  id_time: any;
  hola: boolean = false;
  color= new Color();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.color= this.navParams.get('color');
  	this.time = this.color.time;
  	this.code_color = this.color.code_color[Math.floor(Math.random() * this.color.code_color.length)];

    var that = this;
    var refreshId = setInterval(function() {
		if (that.time==0) {
		    if (that.interval == that.color.repeat) {
		    	clearInterval(refreshId);
		    }else{
		    	that.code_color = that.color.code_color[Math.floor(Math.random() * that.color.code_color.length)];
		    	that.time = that.color.time;
		    	that.interval++;
		    }
		    	        
		}else{
			that.time= that.time -1;  
        }
			  		
	}, 1000);
     

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Coloram');
  }

  public return(){
  	this.navCtrl.setRoot( HomePage );
  }

}
